const TelegramBot = require('node-telegram-bot-api');
const token = '683440246:AAFIi_9mB_jgG4jbhhYF53bZb8AekrdRWh0';
const bot = new TelegramBot(token, { polling: true });

const request = require("request");

var current_user_info = {};
var re_email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

bot.on('polling_error', (error) => {
    console.log(error); // => 'EFATAL'
});

bot.on('message', (msg) => {
    // Various commands to listen to...
    var Hi = "hi";
    var skill_python = "Python";
    var skill_js = "JavaScript";
    var skill_python = "Python";
    var skill_js = "JavaScript";
    var skills_description = "Save the description!"
    var skills_link = "Save the link!";
    var skills_email = "Save the email!"
    var bye = "bye";

    if (msg.text.toString().toLowerCase().indexOf(Hi) == 0) {
        bot.sendMessage(msg.chat.id, "Hello " + msg.from.first_name);
    } else if (msg.text.toString().toLowerCase().includes(bye)) {
        bot.sendMessage(msg.chat.id, "Hope to see you around again!");
    } else if ((msg.text.indexOf(skill_python) == 0)) {
        current_user_info[msg.from.id].selected_skill = "Python";
        current_user_info[msg.from.id].status = "adding_description";
        console.log(current_user_info);
        bot.sendMessage(msg.chat.id, "Cool! You know Python! Now it's time to prove it ;)\n*Let's get started with the claims process then!*\nFirstly, enter a description for your claim. (This will describe what you're claiming here)\nOnce you're done, click on the button below", {
            "reply_markup": {
                "keyboard": [
                    ["Save the description!"],
                ]
            },
            "one_time_keyboard": true
        });
    } else if (msg.text.indexOf(skill_js) == 0) {
        current_user_info[msg.from.id].selected_skill = "JavaScript";
        current_user_info[msg.from.id].status = "adding_description";
        console.log(current_user_info);
        bot.sendMessage(msg.chat.id, "Cool! You know JavaScript! Now it's time to prove it ;)\n*Let's get started with the claims process then!*\nFirstly, enter a description for your claim. (This will describe what you're claiming here)\nOnce you're done, click on the button below", {
            "reply_markup": {
                "keyboard": [
                    ["Save the description!"],
                ]
            },
            "one_time_keyboard": true
        });
    } else if (msg.text.indexOf(skills_description) == 0) {
        if (current_user_info[msg.from.id].description) {
            current_user_info[msg.from.id].status = "adding_link";
            console.log(current_user_info);
            bot.sendMessage(msg.chat.id, "Now you need to add a link to your repo", {
                "reply_markup": {
                    "keyboard": [
                        ["Save the link!"],
                    ]
                },
                "one_time_keyboard": true
            });
        } else {
            bot.sendMessage(msg.chat.id, "You first need to add some description");
        }

    } else if (msg.text.indexOf(skills_link) == 0) {
        if (current_user_info[msg.from.id].repo_link) {
            current_user_info[msg.from.id].status = "adding_email"
            bot.sendMessage(msg.chat.id, "Now give us your email", {
                "reply_markup": {
                    "keyboard": [
                        ["Save the email!"],
                    ]
                },
                "one_time_keyboard": true
            });
        } else {
            bot.sendMessage(msg.chat.id, "You first need to add a link to a repo");
        }

    } else if (msg.text.indexOf(skills_email) == 0) {
        if (current_user_info[msg.from.id].claim_email) {
            current_user_info[msg.from.id].status = "verifying_email"
            bot.sendMessage(msg.chat.id, "That's great! We will now send you a link to verify your email. Once you do that, your claim will be posted to our validators!!! Best luck!", {
                "reply_markup": {
                    "keyboard": [
                        ["Done"],
                    ]
                },
                "one_time_keyboard": true
            });
            var options = {
                method: 'POST',
                url: 'https://ecs-staging-backend.attores.com/graphql',
                headers: {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json'
                },
                body: {
                    operationName: 'mutateCreateClaim',
                    variables: {
                        email: current_user_info[msg.from.id].claim_email,
                        title: 'Javascript',
                        desc: current_user_info[msg.from.id].description,
                        proof: current_user_info[msg.from.id].repo_link,
                        level: 'beginner'
                    },
                    query: 'mutation mutateCreateClaim($email: EmailAddress!, $title: String!, $desc: String!, $proof: URL, $level: ClaimLevel!) {\n  createClaimDraft(email: $email, title: $title, desc: $desc, proof: $proof, level: $level) {\n    userExists\n    __typename\n  }\n}\n'
                },
                json: true
            };

            request(options, function(error, response, body) {
                if (error) throw new Error(error);

                console.log(body);
            });
        } else {
            bot.sendMessage(msg.chat.id, "You first need to add an email");
        }
    } else if (current_user_info[msg.from.id]) {
        if (current_user_info[msg.from.id].status) {
            if (current_user_info[msg.from.id].status.includes("adding_description")) {
                current_user_info[msg.from.id].description = msg.text;
                console.log(current_user_info);
                bot.sendMessage(msg.chat.id, `You have added this as your description - ${msg.text}!\nNow please save the description by clicking on the button below`);
            } else if (current_user_info[msg.from.id].status.includes("adding_link")) {
                current_user_info[msg.from.id].repo_link = msg.text;
                console.log(current_user_info);
                bot.sendMessage(msg.chat.id, `You have added this as your link - ${msg.text}!\nNow please save the link by clicking on the button below`);
            } else if (current_user_info[msg.from.id].status.includes("adding_email")) {
                if (re_email.test(msg.text.toString().toLowerCase())) {
                    current_user_info[msg.from.id].claim_email = msg.text;
                    console.log(current_user_info);
                    bot.sendMessage(msg.chat.id, "Great! Now please save the email by clicking on the button below");
                } else {
                    bot.sendMessage(msg.chat.id, "Please enter a valid email");
                }

            }
        }
    }
})

bot.onText(/\/start/, (msg) => {
    current_user_info[msg.from.id] = {};
    console.log(current_user_info);
    bot.sendMessage(msg.chat.id, "Welcome to the Indorse Claims Bot!!! \nYou can validate your claim and stand a chance to earn 50 Indorse tokens.\nTo get started, you can select one of the skills below...", {
        "reply_markup": {
            "keyboard": [
                ["JavaScript"],
                ["Python"]
            ]
        },
        "one_time_keyboard": true
    });

});


bot.onText(/\/sendpic/, (msg) => {
    bot.sendPhoto(msg.chat.id, "https://images.pexels.com/photos/764880/pexels-photo-764880.jpeg?cs=srgb&dl=alone-black-and-white-cool-wallpaper-764880.jpg&fm=jpg", { caption: "Here we go ! \nThis is just a caption " });
});