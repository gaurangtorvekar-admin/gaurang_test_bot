// import { createApolloFetch } from 'apollo-fetch';


// const uri = 'https://ecs-staging-backend.attores.com/graphql';
// const query = `
//   mutation {
//               createClaimDraft(email: "gaurang@indorse.io",
//                               title:"abc",
//                               desc:"desc",
//                               proof:"https://github.com",
//                               level:"begineer"){
//                                 userExists
//                               }
//             }
// `;

// const apolloFetch = createApolloFetch({ uri });
// apolloFetch({query}).then(console.log("Done sending the create draft claim request"));

import 'isomorphic-fetch';
import api, {GraphQLCall} from "graphql-call";
 
let client = api({url: 'https://ecs-staging-backend.attores.com/graphql'});

client.mutation({
    createClaimDraft: {
      variables: {email: "gaurang+test10@indorse.io", title: "abc", desc: "Test description", proof: "https://github.com", level: "begineer"},
      result: 'userExists'
    }
}).then(result => {
  console.log('result: ', result)
});


